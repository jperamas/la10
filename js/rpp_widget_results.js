var rppWR = window.rppWR || {};
window.rppWR = rppWR,
rppWR.version = "0.0.1",
rppWR.name = "Widget Load Data Results - RPP",
rppWR.empty = "",
rppWR.urlBase = "http://eventos.rpp.com.pe/services/datafactory/html/v3/htmlCenter/data/deportes/futbol/",
rppWR.urlShields = "http://cdn.datafactory.la/escudos_ch/",
rppWR.urlShieldsExt = "http://eventos.rpp.com.pe/services/datafactory/escudos/",
rppWR.shieldsExt = "png",
rppWR.isIEVersion = function() {
    var e = navigator.userAgent.toLowerCase();
    return -1 != e.indexOf("msie") ? parseInt(e.split("msie")[1]) : !1
},
rppWR.isIEVersion() <= 9 && !function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function(e) {
    if (!e.support.cors && e.ajaxTransport && window.XDomainRequest) {
        var t = /^https?:\/\//i, a = /^get|post$/i, s = new RegExp("^" + location.protocol, "i");
        e.ajaxTransport("* text html xml json", function(r, p) {
            if (r.crossDomain && r.async && a.test(r.type) && t.test(r.url) && s.test(r.url)) {
                var i = null;
                return {send: function(t, a) {
                        var s = "", n = (p.dataType || "").toLowerCase();
                        i = new XDomainRequest, /^\d+$/.test(p.timeout) && (i.timeout = p.timeout), i.ontimeout = function() {
                            a(500, "timeout")
                        }, i.onload = function() {
                            var t = "Content-Length: " + i.responseText.length + "\r\nContent-Type: " + i.contentType, s = {code: 200,message: "success"}, r = {text: i.responseText};
                            try {
                                if ("html" === n || /text\/html/i.test(i.contentType))
                                    r.html = i.responseText;
                                else if ("json" === n || "text" !== n && /\/json/i.test(i.contentType))
                                    try {
                                        r.json = e.parseJSON(i.responseText)
                                    } catch (p) {
                                        s.code = 500, s.message = "parseerror"
                                    }
                                else if ("xml" === n || "text" !== n && /\/xml/i.test(i.contentType)) {
                                    var o = new ActiveXObject("Microsoft.XMLDOM");
                                    o.async = !1;
                                    try {
                                        o.loadXML(i.responseText)
                                    } catch (p) {
                                        o = void 0
                                    }
                                    if (!o || !o.documentElement || o.getElementsByTagName("parsererror").length)
                                        throw s.code = 500, s.message = "parseerror", "Invalid XML: " + i.responseText;
                                    r.xml = o
                                }
                            } catch (d) {
                                throw d
                            }finally {
                                a(s.code, s.message, r, t)
                            }
                        }, i.onprogress = function() {
                        }, i.onerror = function() {
                            a(500, "error", {text: i.responseText})
                        }, p.data && (s = "string" === e.type(p.data) ? p.data : e.param(p.data)), i.open(r.type, r.url), i.send(s)
                    },abort: function() {
                        i && i.abort()
                    }}
            }
        })
    }
}),
rppWR.loadData = function(e, t, a) {
    var s = {events: "",type: "",matchId: ""};
    s = $.extend(s, t);
    var r = {},
    _noc = (new Date()).getTime();
    if ("" != s.events && "" != s.type) {
        var p = s.type.toLowerCase();
        if ($.support.cors = !0, "agendamam" === p)
            $.getJSON(rppWR.urlBase + s.events + "/" + s.type + "/es/agenda.json?v="+_noc, function(t) {
                t = t;
                var s = e;
                a(t, s)
            });
        else if ("mam" === p && "" != s.matchId)
            $.getJSON(rppWR.urlBase + s.events + "/events/" + s.matchId + "." + p + ".json?v="+_noc, function(t) {
                t = t;
                var s = e;
                a(t, s)
            });
        else {
            var i = e;
            a(r, i)
        }
    } else {
        var i = e;
        a(r, i)
    }
},
rppWR.ifExist = function(e, t) {
    {
        var a = e, s = t;
        rppWR.empty
    }
    return null != a ? a : s
},
rppWR.sizeJSON = function(e) {
    var t = e;
    return Object.keys || (Object.keys = function(e) {
        var t = [];
        for (var a in e)
            e.hasOwnProperty(a) && t.push(a);
        return t
    }), Object.keys(t).length
},
rppWR.loader = '<div class="loader_widget"></div>',
rppWR.buildAgendaMaM = function(e, t, a) {
    var e = e, t = t, s = e.events, a = a, r = $(t.id);
    if (r.addClass("rppWR-wrap"), r.html(""), s) {
        var p = 0, i = [], n = (rppWR.sizeJSON(s), "");
        var _sS = rppWR.sizeJSON(s);
        for (key in s) {
            var o = (s[key].statusId, key.split(".")), d = o[o.length - 1];
            p++;
            var l = p;
            r.append('<div class="rppWR-box_result">' + rppWR.loader + "</div>"),
            rppWR.loadData(l, {events: a.events,type: "MaM",matchId: d}, function(e, a) {
                var e = e, s = a;
                if (e) {
                    console.log('alerta');
                    var r = e.match.date.split("");
                    if (r)
                        var p = r.slice(0, 4).join(""), o = r.slice(4, 6).join(""), d = r.slice(6, 8).join(""), l = d + "/" + o /*+ "/" + p*/;
                    else
                        var l = "";
                    var c = e.match.scheduledStart;
                    if (c && c !=':')
                        var m = c.split(":"),
                        v = m[0],
                        u = m[1],
                        txth = 'Hora:',
                        h = txth + "&nbsp;" + (parseInt(v) - 2) + ":" + u;
                    else
                        var h = "";
                        n = '<div data-status="'+e.status.statusId+'" class="rppWR-header_result">' + '<span>' + ((e.match.week).split("-"))[0] + '</span>'+'<span>' + ((e.match.week).split("-"))[1] + '</span>'      + '</div><div class="rppWR-time_result">' + e.match.dayName + " " + l /*+ " &nbsp;" + h*/ + "</div>";
                        var R = parseInt(e.status.statusId);
                        0 === R ? n += '<div class="rppWR-status_result rppWR-statusid-0">Por Iniciar</div>' : 1 === R || 5 === R || 6 === R ? n += '<div class="rppWR-status_result rppWR-statusid-1">En Vivo</div>' : 2 === R && (n += '<div class="rppWR-status_result rppWR-statusid-2">Finalizado</div>'), n += '<table class="rppWR-table"><tbody><tr><td class="rppWR-td1"><img src="' + rppWR.urlShields + e.match.homeTeamId + '.gif" alt="' + e.match.homeTeamName + '" /></td><td class="rppWR-td2">' + e.match.homeTeamName + '</td><td class="rppWR-td3">' + rppWR.ifExist(e.scoreStatus[e.match.homeTeamId].score, "-") + '</td></tr><tr><td class="rppWR-td1"><img src="' + rppWR.urlShields + e.match.awayTeamId + '.gif" alt="' + e.match.awayTeamName + '" /></td><td class="rppWR-td2">' + e.match.awayTeamName + '</td><td class="rppWR-td3">' + rppWR.ifExist(e.scoreStatus[e.match.awayTeamId].score, "-") + "</td></tr></tbody></table>"

                }
                $(t.id).find(".rppWR-box_result").eq(s - 1).html(n), i.push([s, n])
                //Finsh last AJAX JSON
                if(i.length === _sS){
                    var _fF = $(t.id).find(".rppWR-header_result");
                    //Show Match - Por iniciar -
                    _fF.each(function(_i,_v){
                        var _vSt = $(_v).data('status');
                        if(_vSt != '2'){
                            var _iI = _fF.index($(_v));
                            var _owl = $(t.id).data('owlCarousel');
                            _owl.goTo(parseInt(_iI));
                            return false;
                        }
                    });
                }
            })
        }
        var c = r;
        r.owlCarousel({items: 3,navigation: !0,slideSpeed: 300,paginationSpeed: 400,autoPlay: !0}), c.find(".owl-prev").html('<i class="icon icon-angle-left"></i>'), c.find(".owl-next").html('<i class="icon icon-angle-right"></i>');
    }
}, rppWR.read = function(e) {
    var e = e;
    return {
        render: function(t) {
            var t = t, a = 0;
            $(t.id).html(rppWR.loader), rppWR.loadData(a, e, function(a) {
                var a = a;
                rppWR.buildAgendaMaM(a, t, e)
            })
        },renderInterval: function(t) {
            var t = t, a = 0, s = $(t.id);
            s.html(rppWR.loader), rppWR.loadData(a, e, function(e) {
                var e = e;
                if (e) {
                    var a = null != e.scoreStatus[e.match.homeTeamId].score ? e.scoreStatus[e.match.homeTeamId].score : rppWR.empty, r = null != e.scoreStatus[e.match.awayTeamId].score ? e.scoreStatus[e.match.awayTeamId].score : rppWR.empty, p = t.url ? t.url : "", i = "copainca" === e.match.channel ? "Torneo del Inca" : e.match.competition, n = a === rppWR.empty && r === rppWR.empty ? "vs" : "x", o = '<div class="rppWR-cntMatch"><table><tbody><tr><td class="rppWR-td1"><div><figure><img src="' + rppWR.urlShieldsExt + e.match.homeTeamId + "." + rppWR.shieldsExt + '" alt="' + e.match.homeTeamName + '" /></figure></div><div>' + e.match.homeTeamName + '</div></td><td class="rppWR-td2"><div class="rppWR-center">', d = parseInt(e.status.statusId);
                    o += 0 === d ? '<div class="rppWR-info-vs rppWR-info-0"><a href="' + p + '"><span class="rppWR-type"> <i class="futbolicon-por-iniciar"></i> POR INICIAR </span><span class="rppWR-league">' + i + "</span></a></div>" : 2 === d ? '<div class="rppWR-info-vs rppWR-info-2"><a href="' + p + '"><span class="rppWR-type"> <i class="futbolicon-finalizado"></i> FINALIZADO </span><span class="rppWR-league">' + i + "</span></a></div>" : '<div class="rppWR-info-vs"><a href="' + p + '"><span class="rppWR-type"> <i class="futbolicon-en-vivo"></i> EN VIVO </span><span class="rppWR-league">' + i + "</span></a></div>", o += '<div class="rppWR-vs-resutls"><a href="' + p + '"><span class="rppWR-vs-goal">' + a + '</span><span class="rppWR-vs-separator">' + n + '</span><span class="rppWR-vs-goal">' + r + '</a></span></div></td><td class="rppWR-td3"><div><figure><img src="' + rppWR.urlShieldsExt + e.match.awayTeamId + "." + rppWR.shieldsExt + '" alt="' + e.match.awayTeamName + '" /></figure></div><div>' + e.match.awayTeamName + "</div></td></tr></tbody></table></div>", s.html(o);

                }
            })
        },renderMaM: function(t) {
            var t = t,
            url = t.url,
            a = 0,
            s = $(t.id),statusId
            empty = '';
            s.html(rppWR.loader);
            rppWR.loadData(a, e, function(e) {
                var e = e;
                if(e){
                    var isMatch = e.match;
                    if(isMatch){
                        //DATE
                        var r = isMatch.date.split("");
                        if (r){
                            var p = r.slice(0, 4).join(""),
                            o1 = r.slice(4, 6).join(""),
                            d = r.slice(6, 8).join(""),
                            l = d + "/" + o1  + "/" + p ; //esta comentado el anio
                        }else{
                            var l = "";
                        }
                        //HORA
                        var c = isMatch.scheduledStart;
                        if (c && c !=':'){
                            var m = c.split(":"),
                            v = m[0],
                            u = m[1],
                            txth = '',
                            h = txth + "&nbsp;" + (parseInt(v) - 2) + ":" + u;
                        }else{
                            var h = "";
                        }
                        //ARBITRO
                        var arb = '', arrArb = [];
                        $.each(e.officials, function(i, el) {
                            arrArb.push(el.name.first + ' ' + el.name.last);
                        });
                        arb = (arrArb.length>0)?arrArb.join('<br />'):'';
                        //Goles
                        var gol1 = '', gol2 = '', arrGol1 = [], arrGol2 = [];
                        $.each(e.incidences.goals, function(i1, el1) {
                            if(el1.team === isMatch.awayTeamId){
                                arrGol2.push(el1.plyrName + ' (' + el1.t.m + '")');
                            }else if(el1.team === isMatch.homeTeamId){
                                arrGol1.push(el1.plyrName + ' (' + el1.t.m + '")');
                            }
                        });
                        gol1 = (arrGol1.length>0)?arrGol1.join('<br />'):'';
                        gol2 = (arrGol2.length>0)?arrGol2.join('<br />'):'';
                        //GOLA TEAM
                        golEq1 = (e.scoreStatus[isMatch.homeTeamId].score===null)?'-':e.scoreStatus[isMatch.homeTeamId].score;
                        golEq2 = (e.scoreStatus[isMatch.awayTeamId].score===null)?'-':e.scoreStatus[isMatch.awayTeamId].score;
                        var pe = e.scoreStatus;

                        //Estados 
                        d = parseInt(e.status.statusId);
                        if(d == 0){
                            d = '<div class="rppWR-info-vs rppWR-info-0"><a href="' + url + '"><span class="rppWR-type"> <i class="futbolicon-por-iniciar"></i> POR INICIAR </span></a></div>';
                        }else if(d == 1){
                            d = '<div class="rppWR-info-vs"><a href="' + url + '"><span class="rppWR-type"> <i class="futbolicon-en-vivo"></i> EN VIVO </span></a></div>';
                        }else if(d == 2){
                            d = '<div class="rppWR-info-vs rppWR-info-2"><a href="' + url + '"><span class="rppWR-type"> <i class="futbolicon-finalizado"></i> FINALIZADO </span></a></div>';
                        }
                        var oHTML = 
                            '<div class="rppWR-cntMatch">'+
                                '<table>'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<td class="rppWR-tdh1" colspan="3">'+
                                                '<span>'+ isMatch.week +'</span>'+
                                                '<time>'+ ' ' + isMatch.dayName + ' ' + l + ' ' + h  + '</time>'+
                                            '</td>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                        '<tr>'+
                                            '<td class="rppWR-td1">'+
                                                '<div>'+
                                                    '<figure><img src="'+ rppWR.urlShieldsExt + isMatch.homeTeamId +'.svg" alt="'+isMatch.homeTeamName+'" width="42" height="42"/></figure>'+
                                                '</div>'+
                                                '<div>'+ isMatch.homeTeamName +'</div>'+
                                            '</td>'+
                                            '<td class="rppWR-td2">'+
                                                '<div class="rppWR-center">'+
                                                    d +
                                                    '<div class="rppWR-vs-resutls">'+
                                                        '<a href="">'+
                                                            '<span class="rppWR-vs-goal">'+ golEq1 +'</span>'+
                                                            '<span class="rppWR-vs-separator">x</span>'+
                                                            '<span class="rppWR-vs-goal">'+ golEq2 +'</span>'+
                                                        '</a>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</td>'+
                                            '<td class="rppWR-td3">'+
                                                '<div>'+
                                                    '<figure><img src="'+ rppWR.urlShieldsExt + isMatch.awayTeamId +'.svg" alt="'+isMatch.awayTeamName+'" width="42" height="42"/></figure>'+
                                                '</div>'+
                                                '<div>'+ isMatch.awayTeamName +'</div>'+
                                            '</td>'+
                                        '</tr>'+
                                    '</tbody>'+
                                '</table>'+
                            '</div>';
                            console.log(e, 'que funcione');
                            console.log(pe);




                        /*'<div class="wrapper-cntest">'+
                          '<div class="info-cntest">'+
                            '<div class="fase-info">'+
                              '<p>Fase:&nbsp;</p>'+
                              '<p>'+ isMatch.week +'</p>'+
                            '</div>'+
                            '<div class="fecha-info">'+
                              '<p>Fecha:&nbsp;</p>'+
                              '<time>'+ ' ' + isMatch.dayName + ' ' + l + ' ' + h  + '</time>'+
                            '</div>'+   
                            // '<div class="arbitro-info">'+
                            //   '<p>Arbitro:&nbsp;</p>'+
                            //   '<p>'+ arb +'</p>'+
                            // '</div>'+
                          '</div>'+
                          '<div class="equipos-cntest">'+
                            '<div class="vs"><img src="/tmp/img/bg-title.png"/><span>vs</span></div>'+
                            '<div class="text-equipos"><span>'+ isMatch.homeTeamName +'</span></div>'+
                            '<div class="text-equipos"><span>'+ isMatch.awayTeamName +'</span></div>'+
                          '</div>'+
                          '<div class="detalle-cntest">'+
                            '<div class="item-detalle-cntest">'+
                              '<div class="nombre">'+
                                '<ul>'+
                                  '<li>'+ gol1 +'</li>'+
                                '</ul>'+
                              '</div>'+
                              '<div class="bandera"><img src="'+ rppWR.urlShieldsExt + isMatch.homeTeamId +'.svg" alt="'+isMatch.homeTeamName+'" width="42" height="42"/></div>'+
                              '<div class="goles"><span>'+ golEq1 +'</span></div>'+
                            '</div>'+
                            '<div class="item-detalle-cntest">'+
                              '<div class="goles"><span>'+ golEq2 +'</span></div>'+
                              '<div class="bandera"><img src="'+ rppWR.urlShieldsExt + isMatch.awayTeamId +'.svg" alt="'+isMatch.awayTeamName+'" width="42" height="42"/></div>'+
                              '<div class="nombre">'+
                                '<ul>'+
                                  '<li>'+ gol2 +'</li>'+
                                '</ul>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>';*/
                        // console.log(e, 'que funcione'); //toda la data enviada
                        s.html(oHTML);
                            
                    }
                }
            });
        }
    };
}, rppWR.tabsResult = function(e, t) {
    var a = $(e), s = t;
    if ("select" === s) {
        var r = $("option:selected", a), p = r.data("id"), i = a.val(), n = a.prev(), o = $(p), d = r.data("events"), l = $("option", a), c = (r.data("id"), n.children("li"));
        c.removeClass("active"), c.eq(i).addClass("active")
    } else {
        {
            var p = a.data("id"), o = $(p), d = a.data("events"), m = a.data("value"), v = a.parent().next(), l = a.parent().find("li");
            l.data("id")
        }
        l.removeClass("active"), a.addClass("active"), v.val(m)
    }
    l.each(function(e, t) {
        $($(t).data("id")).css("display", "none")
    }), o.css("display", "block"), o.children(".owl-wrapper-outer").size() <= 0 && o.html('<script charset="utf-8">rppWR.read({events: "' + d + '", type: "agendaMaM", matchId: ""}).render({id:"' + p + '"});</script>')

    //Focus
    //Finsh last AJAX JSON
    var _fF = $(p).find(".rppWR-header_result");
    //Show Match - Por iniciar -
    if(_fF){
        _fF.each(function(_i,_v){
            var _vSt = $(_v).data('status');
            if(_vSt != '2'){
                var _iI = _fF.index($(_v));
                var _owl = $(p).data('owlCarousel');
                _owl.goTo(parseInt(_iI));
                return false;
            }
        });
    }
    //End Focus
}, rppWR.rppGallery = function(obj)
    {
      var obj = obj,
      size = obj.items.length,
      id = $(obj.id),
      count = 1;
      var html = '<ul class="gallery-list">';   

      for (var i = 0; i < size; i++) 
      {

        html += 
        '<li class="item">' +
          '<figure>' +
            '<img src="' + obj.items[i].src + '" width="825"/></figure>' +
            '<figcaption>' +
              '<strong>' + obj.items[i].description + '</strong>' +
              '<div class="paginador">' +
                '<div class="creditos">' +
                  '<p>Créditos: </p>' +
                  '<span>' +obj.items[i].credito + '</span>' +
                '</div>' +
                '<div class="pagers-legend-gallery">' +
                  '<i class="icon icon-picture-o"></i>' +
                  '<span>' + count + ' de ' + size + '</span>' +
                '</div>' +
              '</div>' +
            '</figcaption>' +
          '</figure>' +
        '</li>' ;
        count = count + 1;
        
      }
      html += '</ul>';
      id.html(html);
      $(obj.id + ' ul').owlCarousel({
        singleItem: true,
        navigation: true,
        addClassActive: true
      });

    };
