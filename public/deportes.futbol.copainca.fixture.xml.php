<?php

class XmlToJson {
	public function Parse ($url, $type) {
		$fileContents= file_get_contents($url);
		$fileContents = str_replace(array("\n", "\r", "\t"), '', $fileContents);
		$fileContents = trim(str_replace('"', "'", $fileContents));
		$simpleXml = simplexml_load_string($fileContents);
		$json = json_encode($simpleXml);
		return 'rppWidget.'.$type.'.data(' . $json . ');';
	}
}
//include 'XmlToJson.php';
header('Content-Type: application/javascript');
echo XmlToJson::Parse('deportes.futbol.copainca.fixture.xml', 'copainca_fixture');

?>
