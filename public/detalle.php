<?php

include_once("_micfg.php");
include_once($root."/mi/mi_all.php");
include_once("common/functions-json.php");

$nid=$mi_ses->ReadHTTPVar("nid"); if ($nid =='NULL')  { header("Location: http://www.ladiez.pe"); die(); }
$fotoG=$mi_ses->ReadHTTPVar("f"); if ($fotoG=='NULL') $fotoG = "1";

$REQUESTPAGE="detalle";
$REQUESTPAGECACHE=$REQUESTPAGE."-$nid-$fotoG.txt";

$script_encuesta = "";

global $CMSROOTURL,$CACHEREFRESHGLOBAL,$CACHEDETAIL,$secid,$titulo_modulo_ultimo;

$mi_ses->CacheControl($CACHEREFRESHGLOBAL);
$mi_ses->Begin( $CACHEDETAIL );

include_once("adv.php");

$url_nd = 'txt/jsons/ndetalles/';
  global $CMSROOTURL,$CACHEREFRESHGLOBAL,$CACHEDETAIL;

  $data = jsonService_($nid,2,0,"rpp");

//---------------------------Obtener Datos----------------------------------
$jsvideos = "";
$titulo= "";
if(isset($data))
{

  $param = $data[0];
  $nid = isset($param["nid"])?$param["nid"]:"";
  $timestamp    = isset($param["timestamp"])?$param["timestamp"]:"";
  $hora         = isset($param["hora"])?$param["hora"]:"";
  $type       = isset($param["tipo"])?$param["tipo"]:"";
  $modelo       = isset($param["modelo"])?$param["modelo"]:"";
  $sid          = isset($param["sid"])?$param["sid"]:"";
  $arr_seccion      = isset($param["seccion"])?explode("/", $param["seccion"]):"";
  $seccion      = "<a href='#'>".$arr_seccion[count($arr_seccion)-1]."</a>";
  $seoseccion      = isset($param["seoseccion"])?$param["seoseccion"]:"";
  $titular      = isset($param["titular"])?$param["titular"]:"";
  $titularcorto = isset($param["titularcorto"])?$param["titularcorto"]:"";
  $usuario      = isset($param["usuario"])?$param["usuario"]:"";
  $linkseo      = isset($param["linkseo"])?$param["linkseo"]:"";
  $creditos     = isset($param["creditos"])?$param["creditos"]:"";
  $linkimg      = isset($param["linkimg"])?$param["linkimg"]:"";
  $linkimg      = getimgseo($linkimg,$titularcorto);
  $link_foto    = "<figure class='media full'><img src='$linkimg' alt='$titular'><figcaption class='help-media'>$creditos</figcaption></figure>";
  //$linkminiimg  = isset($param["linkminiimg"])?$param["linkminiimg"]:"";
  //$linkcropimg  = isset($param["linkcropimg"])?$param["linkcropimg"]:"";
  $elemento     = isset($param["elemento"])?$param["elemento"]:"";
  $fuentenoticia = isset($param["fuentenoticia"])?$param["fuentenoticia"]:"";
  $fuentefoto   = isset($param["fuentefoto"])?$param["fuentefoto"]:"";
  $keywords     = isset($param["keywords"])?$param["keywords"]:"";
  $gorro        = isset($param["gorro"])?$param["gorro"]:"";
  $comenta      = isset($param["comenta"])?$param["comenta"]:"";
  $bullets      = isset($param["bullets"])?$param["bullets"]:"";
  $idsrelacionadas  = isset($param["idsrelacionadas"])?$param["idsrelacionadas"]:"";
  $elementomovil  = isset($param["elementomovil"])?$param["elementomovil"]:"";
  $citas        = isset($param["citas"])?$param["citas"]:"";
  $seotitular   = isset($param["seotitular"])?$param["seotitular"]:"";
  $desarrollo   = isset($param["desarrollo"])?$param["desarrollo"]:"";
  $incrustados  = isset($param["incrustados"])?$param["incrustados"]:"";
  $embedurls    = isset($param["embedurls"])?$param["embedurls"]:"";
  $secrelacionadas = isset($param["secrelacionadas"])?$param["secrelacionadas"]:"";
  $seccionruta  = isset($param["seccionruta"])?$param["seccionruta"]:"";
  $sitio        = isset($param["sitio"])?$param["sitio"]:"";
  $crudo        = isset($param["crudo"])?$param["crudo"]:"";
  $embed        = ($param["embed"]!="*")?$param["embed"]:"";


if($keywords !=""){
  $tags = "";
  $arr_tags = explode(",", $keywords);
  foreach ($arr_tags as $key => $value) {
    $url = seotitle(trim($value))."_busqueda";
    $tags .= "<h2><a href='/".$url."'>$value</a></h2>";
  }
}
if ($idsrelacionadas !="") {
  $nota_rel = "<section class='related-news-full module module-blue'><h3>Noticias relacionadas</h3> <ul class='title-top'>";
  $arr_relacionadas = explode(",", $idsrelacionadas);
  foreach ($arr_relacionadas as $key => $value) {
    if($value!="" or $value != null){
      $info_rel = jsonService_($value,2,0,"rpp");

      if($info_rel!=null){
          $content_rel = $info_rel[0];
          $titularrel      = isset($content_rel["titular"])?$content_rel["titular"]:"";
          $linkimgrel      = isset($content_rel["linkimg"])?str_replace("-1", "7", $content_rel["linkimg"]):"";
          $linkimgrel      = getimgseo($linkimgrel,$titularrel);
          $linkseorel      = isset($content_rel["linkseo"])?$content_rel["linkseo"]:"";
          $seccionrel      = isset($content_rel["seccion"])?$content_rel["seccion"]:"";
          $seccionrel      = txtsection($seccionrel);
          $nota_rel .= " <li><div class='box'><figure class='media'><a href='#'><img src='$linkimgrel' alt='$titularrel'></a></figure><h3 class='tag-title'>$seccionrel</h3><h2><span><a href='$linkseorel'>$titularrel</a></span></h2></div></li>";
      }
    }
  }
  $nota_rel .="</ul></section>";
}
$title = $titular;
$pictimagen = $linkimg;
global $title,$gorro,$keywords,$linkseo,$nid,$pictimagen;
//-----------------------Cabecera -------------------
$tmp_cabecera=new MI_Template("tmp/","header.html");
$tmp_cabecera->ReplaceTags("tags_header/");
$tmp_cabecera->ProcessTags("tags_seo/","SEO");
$tmp_cabecera->ProcessTags("tags_header/","LINKS");
$tmp_cabecera->ProcessTags("tags_seo/","SECCIONSEO");
$tmp_cabecera->ReplaceTags("tags_adv/");
$tmp_cabecera->ProcessTags("tags_adv/","ADVZONES");
$tmp_cabecera->ProcessTags("tags_header/","SETCOUNTERS");
$tmp_cabecera->Show($show);

//----------------------Cuerpo-------------------------

$tmp_detalle = new MI_Template("tmp/","detail_2.html");
$tmp_detalle->ReplaceTags("tags_home/");
$tmp_detalle->ReplaceTags("tags_widgets/");
$tmp_detalle->ReplaceTags("tags_dfp/");




  //-------------- TIPO DE NOTICIAS-----------------------------

       // if ($type=='audio'||$type=='video'||$type=='galeria'||$type=='infografia'||$type=='mapa'){

        if ($type=='audio' || $type=='video')
        {
            if (strpos($elemento,'.flv')!==false)
            {
              $elemento=file_get_contents("http://multimedia.rpp.com.pe/convert.php?media=$elemento");
              $elemento=str_replace('.mp4','_O.mp4',$elemento);
            }

            $elemento = str_replace("&streamer=rtmp://media.rpp.com.pe/rppvod", "", $elemento);
            $elemento = str_replace("http://avi.rpp.com.pe", "", $elemento);
            $elemento = str_replace("http://cdn.rpp.com.pe", "", $elemento);
            $elemento = "http://cdn.rpp.com.pe".$elemento;
            $player= "<script>setJW6('swfplayer', '$elemento', '$linkimg');</script>";
          //$tmp_detalle->Replace("[PLAYER_FOTOS]","");
          $tmp_detalle->Replace("[BOXENCUESTA]","");
        }
        else if ($type=='galeria')
        {
          $li_embed="<div id='mrelacionado$nid' class='news_media_s'><a id='lightboxmm$nid' href='widgets/fotos/?nid=$nid'><img src='$linkimg' /><span class='icon_$type-32x32-2'></span></a><h4>$title| $fuentefoto</h4></div>
                        <script type='text/javascript'>
                        $('#lightboxmm$nid').fancybox({ 'width'  : '7',
                            'height'      : '7',
                                'autoScale'       : false,
                                'transitionIn'    : 'none',
                            'transitionOut'   : 'none',
                            'scrolling'     : 'no',
                            'type'        : 'iframe'});
                        </script>";
          $foto='';
          $player="<span class='gallery-trigger prev'> <a class='left carousel-control' href='#' onclick='beforeP(); return false;' data-slide='prev'><i class='icon icon-angle-left'></i></a></span><span class='gallery-trigger next'><a class='right carousel-control' href='#' onclick='nextP(); return false;' data-slide='next'><i class='icon icon-angle-right'></i></a></span>";
           $link = $linkseo;$seccion_seo = $seoseccion;
           global $nid,$elemento,$link,$fotoG,$hostname,$seccion_seo,$keywords;
           global $title,$type,$fuente_noticia,$creditos;
           global $gorro,$linkseo,$pictimagen;
          $tmp_detalle->Replace("[NID]",$nid);
          $tmp_detalle->Replace("[EPL_300X250]",'');
          $tmp_detalle->Replace("[EPL_300X100]",'');
          $tmp_detalle->ReplaceTags("tags_detalle/");
          $tmp_detalle->Replace("[BOXENCUESTA]","");
        }elseif ($type=='encuesta')
        {
          $script_encuesta = '<script src="//s3.amazonaws.com/brandcaptcha-st1/js/bcaptcha.js"></script>';
          $tmp_encuesta=new MI_Template("tmp/","m_detalle_encuesta.html");
          $desarrollo=explode('|', $desarrollo);
          $lista_alternativa=''; $resultados='';

          for($al=1;$al<count($desarrollo);$al++)
          {
            $tmp_encuesta_li=new MI_Template("tmp/","m_detalle_encuesta-opcion.html");
            $tmp_encuesta_li->Replace("[OPCION]",$desarrollo[($al-1)]);
            $tmp_encuesta_li->Replace("[OPCION_ORDEN]",$al-1);
            $tmp_encuesta_li->Replace("[NID]",$nid);
            /*$lista_alternativa.=
            '<div class="input"><input type="radio" value="'.($al-1).'" id="alternativa'.($al-1).'" name="alternativa'.$nid.'"/><a
             class="txt_t14-verde" onclick=javascript:document.getElementById("alternativa'.($al-1).'").checked=true;>'.$desarrollo[($al-1)].'</a></div>';*/
             $lista_alternativa .= $tmp_encuesta_li->Content();
          }

          //------------ MOSTRAR RESULTADOS ---------------
          //------------ SUMAR VOTOS ----------------------
            $name_file_noticia = $nid."-votos.txt";
            $ruta = "txt/votos/".$name_file_noticia;
            if(!file_exists($ruta))
            {
              $file = fopen("$ruta", "w");
              fwrite($file, "1-0|0|0|0|0");
              fclose($file);
            }

            $file_noticia = file("$ruta");
            $opcion=explode("-",$file_noticia[0]);
              $pregunta=$opcion[0];
            $alternativas=$opcion[1];
            $rpta=explode('|',$alternativas);

            //-----------------------------------------------
            $value=unserialize(file_get_contents('txt/jsons/ndetalles/'.$nid.'_la10.txt'));
            if (!empty($value))
            {
              $optiones = $value["desarrollo"];
              $optiones=explode('|', $optiones);
              $tvotes=$rpta[0] + $rpta[1] + $rpta[2]+ $rpta[3]+ $rpta[4];
              $resultados='';$opt=0;
              $re=0;

              while ($optiones[$re] != "...END")
              {
                if ($tvotes!=0)$pvotes=$rpta[$re]*100/$tvotes; else $pvotes=$rpta[$re]*100;
                if ($re==0) $mayor=$pvotes;
                if ($mayor<$pvotes) {$mayor=$pvotes; $opt=$re;}
                $resultados.='


                <div class="claseprogreso progress [CLASS'.$re.']">
                  <div class="progress-bar progress-bar-success progress-bar-lg" role="progressbar" aria-valuenow="'.round($pvotes).'" aria-valuemin="0" aria-valuemax="100" style="width: '.round($pvotes).'%;">
                  </div>
                  <div class="progress-in">
                    <strong>'.round($pvotes).'% </strong> | '.$optiones[$re].'
                  </div>
                </div>';

                $re++;
              }

              $resultados=str_replace("[CLASS$opt]","bg_win",$resultados);

              for ($i=0;$i<5;$i++)
              {
                $resultados=str_replace("[CLASS$i]","bg_fail",$resultados);
              }

              $result=$resultados.'<h3 class="txt_a18-negro">Total de votos: '.$tvotes.'</h3>';

          }


          /////////
          $tmp_detalle->Replace("[OPCIONES]","");
          $tmp_detalle->Replace("[ONCLICK_VOTAR]","javascript:sendvotes(2,$nid);");
          $random=rand(1000,5000);

                  $jsencuesta="
          <div id=\"claseprogreso$nid\"></div>
          <script>
            function encuestachange(sthis,nid){

            var selec='';
            var activo=0;
            page='votar.php?v=2&alt='+sthis.value+'&nid='+nid;

              $.ajax({
                url: page
              }).done(function(data) {


                  $('#claseprogreso'+nid).html(data);
                $('.claseinput'+nid).hide();
              });

          }
          </script>";
          $tmp_encuesta->Replace("[NID]",$nid);
          $tmp_encuesta->Replace("[ENCUESTA_OPCION]",$lista_alternativa.$jsencuesta);
          $tmp_encuesta->Replace("[ENCUESTA_RESULTADO]",$result);
          $desarrollo="";
          $crudo="";
          $jsencuesta =  $tmp_encuesta->Content();
          $tmp_detalle->Replace("[PLAYER_FOTOS]","");
          $tmp_detalle->Replace("[BOXENCUESTA]",$jsencuesta);
        }

$tmp_detalle->Replace("[SECCION01]",$seccion);
$tmp_detalle->Replace("[FECHA01]",formatdate($timestamp)." | ".$hora);
$tmp_detalle->Replace("[TITULO01]",$titular);
$tmp_detalle->Replace("[GORRO01]",$gorro);
$tmp_detalle->Replace("[CREDITOS01]",$creditos);
$tmp_detalle->Replace("[FOTO01]",$link_foto);
$tmp_detalle->Replace("[PLAYER01]",$player);
$tmp_detalle->Replace("[JSVIDEOS]",$jsvideos);
$tmp_detalle->Replace("[RELACIONADAS]",$nota_rel);
$tmp_detalle->Replace("[TAGS01]",$tags);
$tmp_detalle->Replace("[DESARROLLO01]",$crudo.$embed);
//$tmp_detalle->Replace("[INCRUSTADO]",$incrustados);
}

$tmp_detalle->Replace("[SECCION01]","");
$tmp_detalle->Replace("[FECHA01]","");
$tmp_detalle->Replace("[TITULO01]","");
$tmp_detalle->Replace("[GORRO01]","");
$tmp_detalle->Replace("[CREDITOS01]","");
$tmp_detalle->Replace("[FOTO01]","");
$tmp_detalle->Replace("[RELACIONADAS]","");
$tmp_detalle->Replace("[TAGS01]","");
$tmp_detalle->Replace("[DESARROLLO01]","");
$tmp_detalle->Replace("[INCRUSTADO]","");


/*$tmp_detalle->ProcessTags("tags_widgets/","WIDGETS");
$tmp_detalle->ProcessTags("tags_widgets/","DUMYPHOTOS");
$tmp_detalle->ProcessTags("tags_seo/","SECCIONSEO");*/

$link = "";
$wwwladiez = "http://www.ladiez.pe/".$link;
	$retweet_big='<a href="http://twitter.com/share" class="twitter-share-button" data-count="vertical" data-via="La10Pe" data-lang="es">Twittear</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>';
    $fbshare_big="<div class='fb-share-button' data-href='$wwwladiez' data-layout='box_count'></div>";
    $googlemas1_big='<script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script><g:plusone size="tall"></g:plusone>';

    $redessociales = "<section class='social-detail'><ul><li><a  href='javascript: void(0);' onclick=\"window.open('http://www.facebook.com/sharer.php?u=".$wwwladiez."','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');\" class=\"facebook\"><i class=\"icon icon-facebook\"></i></a></li>
<li><a  href=\"javascript: void(0);\" onclick=\"window.open('https://twitter.com/intent/tweet?text=&url=".$wwwladiez."&via=ladiez','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');\" class=\"twitter\"><i class=\"icon icon-twitter\"></i></a></li>
<li><a  href=\"javascript: void(0);\" onclick=\"window.open('https://accounts.google.com/ServiceLogin?service=oz&passive=1209600&continue=https://plus.google.com/share?url%3D".$wwwladiez."%26gpsrc%3Dframeless&btmpl=popup','ventanacompartir', 'toolbar=0, status=0, width=650, height=450');\" class=\"gplus\"><i class=\"icon icon-google-plus\"></i></a></li>
<li class='li-whatsapp'><a href='whatsapp://send?text=$titulo - $wwwladiez' class='whatsapp' ><i class='icon icon-whatsapp'></i></a></li></ul></section>";

$jsfb =  "<div id=\"fb-root\"></div> <script>(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;    js.src = \"//connect.facebook.net/es_LA/sdk.js#xfbml=1&appId=287920207998918&version=v2.0\";
  fjs.parentNode.insertBefore(js, fjs);  }(document, 'script', 'facebook-jssdk'));</script>";
$comments_fb = $jsfb."<b:includable id='post' var='post'><b:if cond='data:blog.pageType == &quot;item&quot;'>
        <div id='comentariosFacebook'><div class=\"fb-comments\" data-width=\"100%\" data-numposts=\"5\" data-href='$wwwladiez' data-colorscheme=\"light\"></div></div></b:if>";

$tmp_detalle->Replace("[RETWEET_02]",$retweet_big);
$tmp_detalle->Replace("[GOOGLE+1_02]",$googlemas1_big);
$tmp_detalle->Replace("[FBSHARE_02]",$fbshare_big);
$tmp_detalle->Replace("[COMMENTS]",$comments_fb);
$tmp_detalle->Replace("[REDESSOCIALES]", $redessociales);
$tmp_detalle->Replace("[COUNTNID]","countnid.php?nid=$nid");
$tmp_detalle->Replace("[PLAYER_FOTOS]", "");
$tmp_detalle->Replace("[BOXENCUESTA]", "");

$tmp_detalle->ReplaceTags("tags_adv/");
$tmp_detalle->ProcessTags("tags_adv/","ADVZONES");

$tmp_detalle->Show($show);


$scripts = '<script src="tmp/js/rpp_div_fixed.min.js"></script><script src="tmp/js/lib/cookie.min.js"></script>'.$script_encuesta;
$tmp_piedepagina=new MI_Template("tmp/","footer.html");
$tmp_piedepagina->Replace("[SCRIPTSDETALLE]",$scripts);
$tmp_piedepagina->ReplaceTags("tags_footer/");
$tmp_piedepagina->ProcessTags("tags_header/","SETCOUNTERS");
$tmp_piedepagina->Show($show);


?>