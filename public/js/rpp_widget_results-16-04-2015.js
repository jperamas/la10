var rppWR = window.rppWR || {};
 window.rppWR = rppWR;
 rppWR.version = '0.0.1';
 rppWR.name = 'Widget Load Data XML - RPP';
 rppWR.loadData = function (xmlfile) {
    if (window.XMLHttpRequest){ xhttp=new XMLHttpRequest(); }
    else{ xhttp=new ActiveXObject('Microsoft.XMLHTTP'); }
    xhttp.open('GET',xmlfile,false);
    xhttp.send();
    return xhttp.responseXML;
}
rppWR.empty = '-';
rppWR.ifExist = function(val){
    var _val = val,
    empty = rppWR.empty;
    if(_val.length > 0 &&  _val.indexOf('undefined') === -1){
        if(_val != undefined){
            return _val;
        }else{
            return empty;
        }
    }else{
        return empty;
    }
}

rppWR.readFixture = function(xmlfile){
    //Build & Read
    var xmlDoc = rppWR.loadData(xmlfile),
    x = xmlDoc.getElementsByTagName('fixture'); //Fixture

    //PATH
    var urlShields = 'http://cdn.datafactory.la/escudos_ch/';

    //Return functions
    return {
        renderFixture : function(obj){
            var _id = document.getElementById(obj.id),
            buildHTML = function(_xml){
                var xFecha = xmlDoc.getElementsByTagName('fecha'), //Fecha
                _html = '<section>';
                _html += '<h3>'+ rppWR.ifExist(xmlDoc.getElementsByTagName('campeonatoNombreAlternativo')[0].innerHTML)+'</h3>';

                for (var i=0;i<xFecha.length;i++){
                    //FECHA
                    _html += '<h4>'+ rppWR.ifExist(xFecha[i].getAttributeNode('nombre').value)+'</h4>';

                    for (var j=0;j<xFecha[i].children.length;j++){
                        //PARTIDO
                        var xPartido = xFecha[i].getElementsByTagName('partido')[j];
                        if(xPartido != undefined){
                            var xEstado = xPartido.getElementsByTagName('estado')[0],
                            xLocal = xPartido.getElementsByTagName('local')[0],
                            xGoleslocal = xPartido.getElementsByTagName('goleslocal')[0],
                            xGolesvisitante = xPartido.getElementsByTagName('golesvisitante')[0],
                            xVisitante = xPartido.getElementsByTagName('visitante')[0],
                            xDateArr = (xPartido.getAttributeNode('fecha').value).split(''),
                            xDateYear = xDateArr.slice(0,4).join(''),
                            xDateMonth = xDateArr.slice(4,6).join(''),
                            xDateDay = xDateArr.slice(6,8).join(''),
                            xHour = (xPartido.getAttributeNode('hora').value).split(':'),
                            xHourN = xHour[0] + ':' + xHour[1];

                            _html += '<article class="article box">'+
                                '<div class="box-inner">'+
                                    '<div class="team-vs">'+
                                        '<div class="time-vs">'+ rppWR.ifExist(xDateDay) + '/' +
                                        rppWR.ifExist(xDateMonth) + '/' +
                                        rppWR.ifExist(xDateYear) + ' - ' +
                                        rppWR.ifExist(xHourN) +
                                        '</div>'+
                                        '<div class="shield-vs">'+
                                             '<div class="shield">'+
                                                 '<figure>'+
                                                     '<img style="'+(rppWR.ifExist(xLocal.getAttributeNode('id').value) != rppWR.empty?'':'display:none')+'" src="'+urlShields +
                                                     rppWR.ifExist(xLocal.getAttributeNode('id').value) +
                                                     '.gif" alt="'+ rppWR.ifExist(xLocal.innerHTML) +'" />'+
                                                 '</figure>'+
                                             '</div>'+
                                             '<div class="vs">'+
                                                 '<span>'+rppWR.ifExist(xGoleslocal.innerHTML)+'</span>'+
                                                 '<span>'+(rppWR.ifExist(xGoleslocal.innerHTML) != rppWR.empty?'x':rppWR.empty)+'</span>'+
                                                 '<span>'+rppWR.ifExist(xGolesvisitante.innerHTML)+'</span>'+
                                             '</div>'+
                                             '<div class="shield">'+
                                                 '<figure>'+
                                                     '<img style="'+(rppWR.ifExist(xVisitante.getAttributeNode('id').value) != rppWR.empty?'':'display:none')+'" src="'+urlShields +
                                                     rppWR.ifExist(xVisitante.getAttributeNode('id').value) +
                                                     '.gif" alt="'+rppWR.ifExist(xVisitante.innerHTML)+'" />'+
                                                 '</figure>'+
                                             '</div>'+
                                         '</div>'+
                                         '<div class="team-vs-name">'+
                                             '<div class="team-name team-left">'+
                                                 '<h2>'+rppWR.ifExist(xLocal.innerHTML)+'</h2>'+
                                             '</div>'+
                                             '<div class="team-name team-right">'+
                                                 '<h2>'+rppWR.ifExist(xVisitante.innerHTML)+'</h2>'+
                                             '</div>'+
                                         '</div>'+
                                         '<div class="time-vs">'+rppWR.ifExist(xEstado.innerHTML)+'</div>'+
                                    '</div>'+
                                '</div>'+
                            '</article>';
                        }
                    };
                };
                _html += '</section>';
                _id.innerHTML = _html;
            }(x);
        }
    }
}
rppWR.readFixture('deportes.futbol.copainca.fixture.xml').renderFixture({id:'cntFixture'});

// Accepts a url and a callback function to run.
/*function requestCrossDomain(site, callback) {

    // If no url was passed, exit.
    if (!site) {
        alert('No site was passed.');
        return false;
    }

    // Take the provided url, and add it to a YQL query. Make sure you encode it!
    var yql = 'http://query.yahooapis.com/v1/public/yql?q=' + encodeURIComponent('select * from xml where url="' + site + '"') + '&format=xml&callback=?';

    // Request that YSQL string, and run a callback function.
    // Pass a defined function to prevent cache-busting.
    $.getJSON(yql, cbFunc);

    function cbFunc(data) {
        // If we have something to work with...
        if (data.results[0]) {
            if (typeof callback === 'function') {
                callback(data);
            }
        }
        // Else, Maybe we requested a site that doesn't exist, and nothing returned.
        else throw new Error('Nothing returned from getJSON.');
    }
}

function callback(data){
    console.log(data.results);
}

requestCrossDomain('http://eventos.rpp.com.pe/services/datafactory/xml/copainca/deportes.futbol.copainca.fixture.xml?callback=?', callback);*/


// programmatically load a script tag on the page using the given url
window.rppWidget = {};
rppWidget.copainca_fixture = {};
function loadRemoteData(url, id) {
  var script = document.createElement("script");
  script.setAttribute("type","text/javascript");
  script.setAttribute("id",id);
  script.setAttribute("src", url);
  document.getElementsByTagName("head")[0].appendChild(script);
}
rppWidget.copainca_fixture.data = function (jsonResult) {
    console.log(jsonResult, 'jsonResult'); //alert the JSON as a string
}
// make a request for the data using the script tag remoting approach.
loadRemoteData("http://deveventos.rpp.com.pe/services/datafactory/json/copainca/deportes.futbol.copainca.fixture.js?callback=0", "xml-live-0");

/*
$.ajax({
    type: "GET",
    url: "http://deveventos.rpp.com.pe/services/datafactory/xml/copainca/deportes.futbol.copainca.fixture.xml",
    dataType: "xml",
    success: function (xml) {
        console.log(xml, 'xml');
    }
});
*/
